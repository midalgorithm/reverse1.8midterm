/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.adsadawut.algorithm1;

import java.util.Arrays;
import java.util.Scanner;

/**
 *
 * @author hanam
 */
public class Reverse {
    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);
        int x  = kb.nextInt();                   //ใส่ตัวเลขเพื่อระบุจำนวนเลขใน array A ที่กำลังจะเกิดขึ้น
        int A[] = new int[x];                    //สร้าง array A สามารถรับข้อมูลได้ x จำนวน
        for(int i=0;i<A.length;i++){         //วนลูปเพื่อใส่เลขลงไปใน array จนกว่าจะครบตามจำนวน x จำนวน
            A[i]=kb.nextInt();
        }
        reverse(A);                                 //เรียกใช้ฟังก์ชันก์ reverse
        for(int i=0;i<A.length;i++){           //วนลูปเพื่อแสดงคำตอบทั้งที่อยู่ใน array
            System.out.print(A[i]+" ");
        }
    }
    public static int[] reverse(int []A){
       int tempNum = 0;
       int last = A.length-1;                                  //กำหนดตำแหน่งของ Index ตัวสุดท้าย   
        for(int i = 0 ; i <Math.floor(A.length/2);i++){ //เช็ค index แต่ละตัวใน array A
            tempNum = A[i];                               // เก็บค่าของตัวเลข ในตำแหน่ง i ไว้ชั่วคราว
            A[i]=A[last-i];                                     //แทนค่าใน array A ตำแหน่งที่ i เป็นค่าของฝั่งตรงข้าม ที่มาจาก array A ตำแหน่งที่ last 
                                                                      //ลดลงมาทีละ i เช่น มีสมาชิกในอัลเลย์ 10 ตัว มี index 0 ถึง 9 ตำแหน่งแรกคือ 0 
                                                                      //ตำแหน่งที่จะทำการสลับคือ 9-0 = 9 
            A[last-i]=tempNum;                           //นำค่า tempNum ที่เก็บค่าใน array A ตำแหน่งที่ i ก่อนหน้า ไปแทนค่า ใน array A ฝั่งตรงข้าม
        }
        return A;
    }
}
